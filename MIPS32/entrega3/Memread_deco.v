module Memread_deco (
	input [31:0] inD,
	input [1:0] ofs,
	input bitX,
	input [1:0] ds,
	output reg [31:0] oD
);

always @ (*) begin
	oD = 32'hx;
	case (ds)
	2'd0: oD = inD; //LOAD WORD
	2'd1: begin //LOAD HALF WORD
		if (ofs == 2'd0 || ofs == 2'd1) begin
			if (bitX)
				oD = { {16{inD[31]}}, inD[31:16] };
			else
				oD = { 16'd0, inD[31:16] };
		end
		else begin
			if (bitX)
				oD = { {16{inD[15]}}, inD[15:0] };
			else
				oD = { 16'd0, inD[15:0] };
		end
	end
	2'd2: begin //LOAD BYTE
		if (ofs == 2'd0) begin
			if(bitX)
				oD = { {24{inD[31]}}, inD[31:24] };
			else
				oD = { 24'd0, inD[31:24] };
		end
		else if (ofs == 2'd1) begin
			if(bitX)
				oD = { {24{inD[23]}}, inD[23:16] };
			else
				oD = { 24'd0, inD[23:16] };
		end
		else if (ofs == 2'd2) begin
			if(bitX)
				oD = { {24{inD[15]}}, inD[15:8] };
			else
				oD = { 24'd0, inD[15:8] };
		end
		else if (ofs == 2'd3) begin
			if(bitX)
				oD = { {24{inD[7]}}, inD[7:0] };
			else
				oD = { 24'd0, inD[7:0] };
		end
	end
	endcase
end

endmodule

module Mem_decoder (
	input [31:0] vAd,
	input mW,
	input mR,
	output reg iAd,
	output [12:0] pAd,
	output reg mE_ram,
	output reg mE_vga,
	output reg mE_io,
	output reg [1:0] mB
);

assign pAd = temp[10:0];
reg [31:0] temp;

always @ (*) begin
	temp = 32'hx;
	iAd = 1'd0;
	mB = 2'd3;
	if (vAd >= 32'h10010000 && vAd < 32'h10011000) begin //global memory
		temp = vAd - 32'h10010000;
		mB = 2'd0;
	end
	else if (vAd >= 32'h7FFFEFFC && vAd < 32'h7FFFFFFC) begin//stack memory
		temp = vAd - 32'h7fffebfc;
		mB = 2'd0;
	end
	else if (vAd >= 32'hB800 && vAd < 32'hCACF) begin //VGA memory
		temp = vAd - 32'hB800;
		mB = 2'd1;
	end
	else if (vAd >= 32'hFFFF0000 && vAd < 32'hFFFF000C) begin //I/O memory
		temp = vAd - 32'hFFFF0000;
		mB = 2'd2;
	end
	else
		if (mR || mW)
			iAd = 1'd1;
end

always @ (*) begin
	mE_ram = 1'd0;
	mE_vga = 1'd0;
	mE_io = 1'd0;
	if (mR || mW) begin
		case (mB)
			2'd0: mE_ram = 1'd1;
			2'd1: mE_vga = 1'd1;
			2'd2: mE_io = 1'd1;
		endcase
	end
end

endmodule

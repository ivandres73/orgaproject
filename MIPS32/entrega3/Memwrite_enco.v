module Memwrite_enco (
	input [31:0] inD,
	input [1:0] ofs,
	input iwe,
	input [1:0] ds,
	output reg [31:0] oD,
	output reg [3:0] owe
);

always @ (*) begin
	oD = 32'dx;
	owe = 4'd0;
	if (iwe) begin
		case (ds)
		2'b00: begin //WORD
			oD = inD;
			owe = 4'b1111;
		end
		2'b01: begin //HALF WORD
			if (ofs == 2'd0 || ofs == 2'd1) begin
				oD = { inD[15:0], 16'd0 };
				owe = 4'b0011;
			end
			else begin
				oD = { 16'd0, inD[15:0] };
				owe = 4'b1100;
			end
		end
		2'b10: begin //BYTE
			if (ofs == 2'd0) begin
				oD = { inD[7:0], 24'd0 };
				owe = 4'b0001;
			end
			else if (ofs == 2'd1) begin
				oD = { 8'd0, inD[7:0], 16'd0 };
				owe = 4'b0010;
			end
			else if (ofs == 2'd2) begin
				oD = { 16'd0, inD[7:0], 8'd0 };
				owe = 4'b0100;
			end
			else if (ofs == 2'd3) begin
				oD = { 24'd0, inD[7:0] };
				owe = 4'b1000;
			end
		end
			
		endcase
	end
end

endmodule

module RAM_decoder (
	input [31:0] Vrtl_addrss,
	output [10:0] Phscl_addrss,
	output reg Invalid_addrss
);

assign Phscl_addrss = temp[12:2];
reg [31:0] temp;

always @ (*) begin
	temp = 32'hx;
	Invalid_addrss = 1'd0;
	if (Vrtl_addrss >= 32'h10010000 && Vrtl_addrss < 32'h10011000) //global memory
		temp = Vrtl_addrss - 32'h10010000;
	else if (Vrtl_addrss >= 32'h7FFFEFFC && Vrtl_addrss < 32'h7FFFFFFC) //stack memory
		temp = Vrtl_addrss - 32'h3fffdffc;
	else
		Invalid_addrss = 1'd1;
end

endmodule

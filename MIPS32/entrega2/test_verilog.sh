#!/bin/bash

MODNAME=$1.v
TESTNAME=${1}_tb.v

iverilog -tvvp -o test $MODNAME $TESTNAME
if [ $? -eq 0 ]; then
    vvp -M /opt/iverilog/lib/ivl test
    rm -f test
fi

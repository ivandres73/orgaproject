module lui (
	input [31:0] rwd,
	input sel,
	output reg [31:0] D_in
);

always @ (*) begin
	D_in = rwd;
	if (sel)
		D_in = { rwd[15:0], 16'd0 };
end

endmodule
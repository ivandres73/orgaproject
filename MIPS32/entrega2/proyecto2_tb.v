//  A testbench for proyecto2_tb
`timescale 1us/1ns

module proyecto2_tb;
    reg clk;
    reg ivan;
    wire [31:0] t_0;
    wire [31:0] t_1;
    wire [31:0] t_2;
    wire [31:0] t_3;
    wire [31:0] ram_0;

  proyecto2 proyecto20 (
    .clk(clk),
    .ivan(ivan),
    .t_0(t_0),
    .t_1(t_1),
    .t_2(t_2),
    .t_3(t_3),
    .ram_0(ram_0)
  );

    reg [97:0] patterns[0:11];
    integer i;

    initial begin
      patterns[0] = 98'b1_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[1] = 98'b1_1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[2] = 98'b1_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[3] = 98'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[4] = 98'b0_1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[5] = 98'b0_0_10101010101110110000000000000000_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[6] = 98'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[7] = 98'b0_1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[8] = 98'b0_0_10101010101110110000000000000000_11001100110111010000000000000000_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[9] = 98'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[10] = 98'b0_1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[11] = 98'b0_0_10101010101110110000000000000000_11001100110111010000000000000000_00010001001000100000000000000000;

      for (i = 0; i < 12; i = i + 1)
      begin
        ivan = patterns[i][97];
        clk = patterns[i][96];
        #10;
        if (patterns[i][95:64] !== 32'hx)
        begin
          if (t_0 !== patterns[i][95:64])
          begin
            $display("%d:t_0: (assertion error). Expected %h, found %h", i, patterns[i][95:64], t_0);
            $finish;
          end
        end
        if (patterns[i][63:32] !== 32'hx)
        begin
          if (t_1 !== patterns[i][63:32])
          begin
            $display("%d:t_1: (assertion error). Expected %h, found %h", i, patterns[i][63:32], t_1);
            $finish;
          end
        end
        if (patterns[i][31:0] !== 32'hx)
        begin
          if (t_2 !== patterns[i][31:0])
          begin
            $display("%d:t_2: (assertion error). Expected %h, found %h", i, patterns[i][31:0], t_2);
            $finish;
          end
        end
      end

      $display("All tests passed.");
    end
    endmodule

module imm_exts (
	input [31:0] imm,
	input sel,
	output reg [31:0] imm_ext
);

always @ (*) begin
	imm_ext = 32'hx;
	if (sel)
		imm_ext = { 16'd0, imm[15:0] };
	else
		imm_ext = { {16{imm[15]}}, imm[15:0] };
end

endmodule

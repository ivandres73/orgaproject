module brancher(
	input beq,
	input bne,
	input bgez,
	input bgtz,
	input blez,
	input bltz,
	input zero,
	input [31:0] r,
	output reg bt
);

wire reg sign = r[31];

always @ (*) begin
	bt = 1'd0;
	if (beq && zero)
		bt = 1'd1;
	else if (bne && ~zero)
		bt = 1'd1;
	else if (bgez && (zero || ~sign))
		bt = 1'd1;
	else if (bgtz && ~sign)
		bt = 1'd1;
	else if (blez && (zero || sign))
		bt = 1'd1;
	else if (bltz && sign)
		bt = 1'd1;
end

endmodule

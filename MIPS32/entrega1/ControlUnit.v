module Control_unit (
  input [5:0] func,
  input [5:0] op,
  output reg Reg_dst,
  output reg Branch_eq,
  output reg Branch_ne,
  output reg Mem_read,
  output reg Mem_toReg,
  output reg [2:0] alu_op,
  output reg Mem_write,
  output reg ALU_src,
  output reg Reg_write,
  output reg jump
);

always @ (*) begin
Reg_dst = 1'd1; // uno es para las instrucciones tipo R
Branch_eq = 1'd0;
Branch_ne = 1'd0;
Mem_read = 1'd0;
Mem_toReg = 1'd0;
Mem_write = 1'd0;
ALU_src = 1'd0;
Reg_write = 1'd0;
jump = 1'd0;
alu_op = 3'dx;
  case (op)
    6'b100011: begin //LW
      Reg_dst = 1'd0;
      Mem_read = 1'd1;
      Mem_toReg = 1'd1;
      alu_op = 3'd0;
      ALU_src = 1'd1;
      Reg_write = 1'd1;
    end
    6'b101011: begin //SW
      alu_op = 3'd0;
      Mem_write = 1'd1;
      ALU_src = 1'd1;
    end
    6'b000100: begin //BEQ
      Branch_eq = 1'd1;
      alu_op = 3'd1;
    end
    6'b000101: begin //BNE
      Branch_ne = 1'd1;
      alu_op = 3'd1;
    end
    6'b000010: jump = 1'd1; //J
    6'b000000: begin
      case (func)
        6'b100000: begin
          alu_op = 3'd0;
          Reg_write = 1'd1;
        end
        6'b100010: begin
          alu_op = 3'd1;
          Reg_write = 1'd1;
        end
        6'b100100: begin
          alu_op = 3'd2;
          Reg_write = 1'd1;
        end
        6'b100101: begin
          alu_op = 3'd3;
          Reg_write = 1'd1;
        end
        6'b101010: begin
          alu_op = 3'd4;
          Reg_write = 1'd1;
        end
        default: begin
  	     Reg_dst = 1'd1; // uno es para las instrucciones tipo R
          Branch_eq = 1'd0;
	     Branch_ne = 1'd0;
	     Mem_read = 1'd0;
	     Mem_toReg = 1'd0;
	     Mem_write = 1'd0;
	     ALU_src = 1'd0;
	     Reg_write = 1'd0;
	     jump = 1'd0;
	     alu_op = 3'dx;
        end
      endcase
    end
    default: begin
      Reg_dst = 1'd1; // uno es para las instrucciones tipo R
      Branch_eq = 1'd0;
      Branch_ne = 1'd0;
      Mem_read = 1'd0;
      Mem_toReg = 1'd0;
      Mem_write = 1'd0;
      ALU_src = 1'd0;
      Reg_write = 1'd0;
      jump = 1'd0;
      alu_op = 3'dx;
    end
  endcase
end

endmodule
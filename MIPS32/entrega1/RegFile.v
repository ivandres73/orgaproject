module RegisterFile(
	input [31:0]Din,
	input we,
	input [4:0]wa,
	input clk,
	input [4:0]ra1,
	input [4:0]ra2,
	output [31:0]rd1,
	output [31:0]rd2,
	output [31:0]t3,
	output [31:0]t4,
	output [31:0]t5,
	output [31:0]t6,
	output [31:0]t7
);

reg [31:0] Registro[0:31];

assign t3 = Registro[3];
assign t4 = Registro[4];
assign t5 = Registro[5];
assign t6 = Registro[6];
assign t7 = Registro[7];

//Para sacar valores del RegisterFile
assign rd1 = Registro[ra1];
assign rd2 = Registro[ra2];

//Para guardar valores en el RegisterFile
always @ (posedge clk) begin
	if (we)
	  Registro[wa] <= Din;
end

initial begin
  Registro[0] <= 32'd0;
  Registro[1] <= 32'd10;
  Registro[2] <= 32'd5;
end

endmodule
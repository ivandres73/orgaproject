module RAM
(
  input [7:0] A,
  input [31:0] Din,
  input str,
  input C,
  input ld,
  output [31:0] D,
  output [31:0] pos0
);
  reg [31:0] memory[0:31];

  assign D = ld? memory[A] : 'hz;
  assign pos0 = memory[0];

  always @ (posedge C) begin
    if (str)
      memory[A] <= Din;
  end
  
  
endmodule
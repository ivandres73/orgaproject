#include "screen.h"

/* EN EL HEADER TNGO ESTAS VARIABLES
uint8_t cursor_row;
uint8_t cursor_col;
uint8_t fg_color;
uint8_t bg_color;
*/

void clear_screen() {
	set_color(0, 1);
	for(int i=0; i < 80; i++) {
		for (int j=0; j < 30; j++) {
			set_cursor(j, i);
			put_char(' ');
		}
	}
	set_cursor(0, 0);
}

void set_cursor(uint8_t row, uint8_t column) {
	cursor_row = row;
	cursor_col = column;
}

void get_cursor(uint8_t *row, uint8_t *column) {
	*row = cursor_row;
	*column = cursor_col;
}

void set_color(uint8_t fgcolor, uint8_t bgcolor) {
	fg_color = fgcolor;
	bg_color = bgcolor;
}

void get_color(uint8_t *fgcolor, uint8_t *bgcolor) {
	*fgcolor = fg_color;
	*bgcolor = bg_color;
}

void put_char(uint8_t ch) {
	uint8_t cursor_row_temp;
	uint8_t cursor_col_temp;
	if (ch == '\n') {
		get_cursor(&cursor_row_temp, &cursor_col_temp);
		set_cursor(cursor_row_temp + 1, 0);
	}
	else {
		uint16_t data = 0x0000 | fg_color;
		data = data << 4;
		data = data | bg_color;
		data = data << 8;
		data = data | ch;

		uint16_t *vgaptr = (uint16_t *)VGA_START_ADDR + cursor_col + (cursor_row*80);
		*vgaptr = data;

		get_cursor(&cursor_row_temp, &cursor_col_temp);
		set_cursor(cursor_row_temp, cursor_col_temp + 1);
	}

}

void puts(char *str) {
	register uint8_t pos = 0;
	do {
		put_char(str[pos++]);
	} while (str[pos] != '\0');
}

void put_decimal(uint32_t n) {
	register uint8_t i = 0;
	char str[100];

	if (n == 0) {
		str[i++] = '0';
		str[i] = '\0';
	}

	while (n != 0) {
		register uint8_t rem = OtherRDivision(n, 10);
		str[i++] = (rem > 9) ? (rem-10) + 'a' : rem + '0';
		n = QDivision(n, 10);
	}

	
	str[i] = '\0';

	char reverse[100];
	register uint8_t v = 0;
	while (i != 0) {
		reverse[v++] = str[--i];
	}

	puts(reverse);
}
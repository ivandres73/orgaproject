//importaciones
#include "system.h"
#include "math.h"

//variables globales
uint8_t cursor_row;
uint8_t cursor_col;
uint8_t fg_color;
uint8_t bg_color;

//funciones
void clear_screen();
void set_cursor(uint8_t row, uint8_t column);
void get_cursor(uint8_t *row, uint8_t *column);
void set_color(uint8_t fgcolor, uint8_t bgcolor);
void get_color(uint8_t *fgcolor, uint8_t *bgcolor);
void put_char(uint8_t ch);
void puts(char *str);
void put_decimal(uint32_t n);
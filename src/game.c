#include "screen.h"

//21 area verde
//22 area negra
//23 piso verdo

struct apples {
	bool eated;
	uint8_t x_pos;
	uint8_t y_pos;
};

bool mapa[13][12];
struct apples a[4];
int8_t clownX = 5;
int8_t clownY = 12;
uint8_t direccion = 0;
uint8_t enemy3_steps = 0;

typedef struct enemy {
	uint8_t x_pos;
	uint8_t y_pos;
	uint8_t direccion;
} enemy;

enemy e1;
enemy e2;
enemy e3;

void initEnemies() {
	e1.x_pos = 5;
	e1.y_pos = 6;
	e1.direccion = 1;

	e2.x_pos = 10;
	e2.y_pos = 2;
	e2.direccion = 3;

	e3.x_pos = 0;
	e3.y_pos = 10;
	e3.direccion = 3;
}

void moveEnemy1() {
	switch (e1.direccion) {
		case 0:
			if (mapa[e1.y_pos][e1.x_pos+1] && (e1.x_pos) < 11)
				e1.x_pos++;
			else {
				e1.direccion = 1;
			}
			break;
		case 1:
			if (mapa[e1.y_pos][e1.x_pos-1] && (e1.x_pos) > 0)
				e1.x_pos--;
			else
				e1.direccion = 2;
			break;
		case 2:
			if (mapa[e1.y_pos-1][e1.x_pos] && (e1.y_pos) > 0)
				e1.y_pos--;
			else
				e1.direccion = 3;
			break;

		case 3:
			if (mapa[e1.y_pos+1][e1.x_pos] && (e1.y_pos) < 12)
				e1.y_pos++;
			else
				e1.direccion = 0;
			break;
	}
}

void moveEnemy2() {
	switch (e2.direccion) {
		case 0:
			if (mapa[e2.y_pos][e2.x_pos+1] && (e2.x_pos) < 11)
				e2.x_pos++;
			else
				e2.direccion = 3;
			break;
		case 1:
			if (mapa[e2.y_pos][e2.x_pos-1] && (e2.x_pos) > 0)
				e2.x_pos--;
			else
				e2.direccion = 2;
			break;
		case 2:
			if (mapa[e2.y_pos-1][e2.x_pos] && (e2.y_pos) > 0)
				e2.y_pos--;
			else
				e2.direccion = 0;
			break;

		case 3:
			if (mapa[e2.y_pos+1][e2.x_pos] && (e2.y_pos) < 12)
				e2.y_pos++;
			else
				e2.direccion = 1;
			break;
	}
}

void moveEnemy3() {
	switch (e3.direccion) {
		case 0:
			if ((mapa[e3.y_pos][e3.x_pos+1] && (e3.x_pos) < 11) && enemy3_steps < 5) {
				e3.x_pos++;
				enemy3_steps++;
			}
			else {
				e3.direccion = 2;
				enemy3_steps = 0;
			}
			break;
		case 1:
			if ((mapa[e3.y_pos][e3.x_pos-1] && (e3.x_pos) > 0) && enemy3_steps < 5){
				e3.x_pos--;
				enemy3_steps++;
			} else {
				e3.direccion = 2;
				enemy3_steps = 0;
			}
			break;
		case 2:
			if ((mapa[e3.y_pos-1][e3.x_pos] && (e3.y_pos) > 0) && enemy3_steps < 5){
				e3.y_pos--;
				enemy3_steps++;
			} else {
				e3.direccion = 1;
				enemy3_steps = 0;
			}
			break;

		case 3:
			if ((mapa[e3.y_pos+1][e3.x_pos] && (e3.y_pos) < 12) && enemy3_steps < 5){
				e3.y_pos++;
				enemy3_steps++;
			} else {
				e3.direccion = 0;
				enemy3_steps = 0;
			}
			break;
	}
}

void renderEnemies() {
	set_color(BLUE, BLACK);
	uint8_t x = e1.x_pos << 1;
	uint8_t y = e1.y_pos;
	set_cursor(y, x);
	switch (e1.direccion) {
		case 0://derecha
			putchar(30);
			putchar(11);
			break;
		case 1://izquierda
			putchar(14);
			putchar(15);
			break;
		case 2://arriba
			putchar(16);
			putchar(17);
			break;
		case 3://abajo
			putchar(12);
			putchar(13);
			break;
	}

	x = e2.x_pos << 1;
	y = e2.y_pos;
	set_cursor(y, x);
	switch (e2.direccion) {
		case 0://derecha
			putchar(30);
			putchar(11);
			break;
		case 1://izquierda
			putchar(14);
			putchar(15);
			break;
		case 2://arriba
			putchar(16);
			putchar(17);
			break;
		case 3://abajo
			putchar(12);
			putchar(13);
			break;
	}

	x = e3.x_pos << 1;
	y = e3.y_pos;
	set_cursor(y, x);
	switch (e3.direccion) {
		case 0://derecha
			putchar(30);
			putchar(11);
			break;
		case 1://izquierda
			putchar(14);
			putchar(15);
			break;
		case 2://arriba
			putchar(16);
			putchar(17);
			break;
		case 3://abajo
			putchar(12);
			putchar(13);
			break;
	}
}

void initApples() {
	a[0].eated = false;
	a[0].x_pos = 7;
	a[0].y_pos = 8;

	a[1].eated = false;
	a[1].x_pos = 3;
	a[1].y_pos = 0;

	a[2].eated = false;
	a[2].x_pos = 4;
	a[2].y_pos = 2;

	a[3].eated = false;
	a[3].x_pos = 8;
	a[3].y_pos = 4;
}

void renderApples () {
	register uint8_t i;
	set_color(YELLOW, GREEN);
	for (i = 0; i < 4; i++) {
		set_cursor(a[i].y_pos, a[i].x_pos << 1);
		if (!(a[i].eated)) {
			putchar(8);
			putchar(9);
		}
	}
}

void eatApples() {
	register uint8_t i;
	for (i = 0; i < 4; i++) {
		if (a[i].x_pos == clownX) {
			if (a[i].y_pos == clownY) {
				a[i].eated = true;
				return;
			}
		}
	}
}

bool ifWin() {
	register uint8_t i;
	for (i = 0; i < 4; i++) {
		if (a[i].eated == false)
			return false;
	}
	return true;
}

bool ifLose() {
	if (e1.y_pos == clownY) {
		if (e1.x_pos == clownX) {
			return true;
		}
	} else if (e2.y_pos == clownY) {
		if (e2.x_pos == clownX) {
			return true;
		}
	} else if (e3.y_pos == clownY) {
		if (e3.x_pos == clownX) {
			return true;
		}
	}
	return false;
}

void levelOne() {
	initApples();
	initEnemies();
	set_color(GREEN, BLACK);
	register uint8_t i;
	for (i=0; i < 13; i++) {
		set_cursor(i, 24);
		putchar('i');
	}
	for (i=0; i < 24; i++) {
		set_cursor(13, i);
		putchar('i');
	}
	for (i=0; i < 13; i++) {
			switch (i) {
			case 0:
				for (register uint8_t v=4; v < 10; v++)
					mapa[i][v] = true;
				break;
			case 1:
				mapa[i][5] = true;
				mapa[i][9] = true;
				mapa[i][10] = true;
				break;
			case 2:
				mapa[i][5] = true;
				mapa[i][10] = true;
				mapa[i][11] = true;
				break;
			case 3:
				for (register uint8_t v=0; v < 7; v++) {
					mapa[i+v][5] = true;
					mapa[i+v][11] = true;
					putchar('\n');
				}
				i = 9;
				continue;
				break;
			case 10:
				mapa[i][0] = true;
				mapa[i][1] = true;
				mapa[i][2] = true;
				mapa[i][5] = true;
				mapa[i][10] = true;
				mapa[i][11] = true;
				break;
			case 11:
				mapa[i][0] = true;
				mapa[i][2] = true;
				mapa[i][5] = true;
				mapa[i][9] = true;
				mapa[i][10] = true;
				break;
			case 12:
				for (register uint8_t v=0; v < 10; v++)
					mapa[i][v] = true;
				break;
			}
	}
}

void render() {
	set_color(GREEN, BLACK);
	set_cursor(0,0);
	for (register uint8_t i=0; i <13; i++) {
		for (register uint8_t v=0; v < 12; v++) {
			if (mapa[i][v]) {
				putchar(22);
				putchar(22);
			} else {
				putchar(21);
				putchar(21);
			}
		}
		putchar('\n');
	}
	renderClown(clownX, clownY, direccion);
	renderApples();
	eatApples();
	renderEnemies();
}

void renderClown(uint8_t x, uint8_t y, uint8_t direccion) {
	mapa[y][x] = true;
	x = x << 1;
	set_cursor(y, x);
	set_color(WHITE, BLACK);
	switch (direccion) {
		case 0://derecha
			putchar(0);
			putchar(1);
			break;
		case 1://izquierda
			putchar(4);
			putchar(5);
			break;
		case 2://arriba
			putchar(6);
			putchar(7);
			break;
		case 3://abajo
			putchar(2);
			putchar(3);
			break;
	}
}

void moveClownRight() {
	clownX++;
	direccion = 0;
	if (clownX > 11) {
		clownX = 11;
		return;
	}
	//delay_ms(50);
}

void moveClownLeft() {
	direccion = 1;
	if (clownX <= 0) {
		clownX = 0;
		return;
	}
	clownX--;
	//delay_ms(50);
}

void moveClownUp() {
	direccion = 2;
	if (clownY <= 0) {
		clownY = 0;
		return;
	}
	clownY--;
	//delay_ms(50);
}

void moveClownDown() {
	clownY++;
	direccion = 3;
	if (clownY > 12) {
		clownY = 12;
		return;
	}
	//delay_ms(50);
}
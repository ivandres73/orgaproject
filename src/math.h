#include "system.h"

uint32_t OtherRDivision (uint32_t numerator, uint32_t denominator);
uint32_t RDivision (uint32_t numerator, uint32_t denominator);
uint32_t QDivision (uint32_t numerator, uint32_t denominator);
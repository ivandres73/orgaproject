#include "screen.h"
#include "game.c"

int main() {

	levelOne();
	while (true) {
		moveEnemy1();
		moveEnemy2();
		moveEnemy3();
		render();
		if (keypad_getkey() == 2)
			moveClownRight();
		else if (keypad_getkey() == 1)
			moveClownLeft();
		else if (keypad_getkey() == 4)
			moveClownUp();
		else if (keypad_getkey() == 3) {
			moveClownDown();
		}

		if (ifWin()) {
			set_cursor(25, 25);
			set_color(GREEN, BLACK);
			puts("YOU WON");
			while (true) {
				set_cursor(26, 20);
				puts("press SPACE to restart");
				if (keypad_getkey() == 8) {
					clear_screen();
					levelOne();
					break;
				}
			}
		}
		if (ifLose()) {
			set_cursor(25, 25);
			set_color(RED, BLACK);
			puts("YOU LOST");
			while (true) {
				set_cursor(26, 20);
				puts("press SPACE to restart");
				if (keypad_getkey() == 8) {
					clear_screen();
					levelOne();
					break;
				}
			}
		}


		delay_ms(100);
	}

	return 0;
}
#include "math.h"

uint32_t OtherRDivision (uint32_t numerator, uint32_t denominator) {
	while (numerator >= denominator) {
  		numerator -= denominator;
	}
	return numerator;
}

uint32_t RDivision (uint32_t numerator, uint32_t denominator) {
	if (denominator == 0) return -1;

	uint32_t R = 0;
	uint32_t bit_i_numerator;
	for (register uint8_t i = 31; i >= 0; i--) {//Where n is number of bits in N
	  R = R << 1;//Left-shift R by 1 bit

	  bit_i_numerator = 0x0 | numerator;
	  bit_i_numerator = bit_i_numerator << (31 - i);
	  bit_i_numerator = bit_i_numerator >> 31;

	  R = R | (bit_i_numerator & 1);//Set the least-significant bit of R equal to bit i of the numerator
	  if (R >= denominator) {
	    R -= denominator;
	  }
	}

	return R;
}

uint32_t QDivision (uint32_t numerator, uint32_t denominator) {
	if (denominator == 0) return -1;

	uint32_t R = 0;
	uint32_t Q = 0;
	uint32_t bit_i_numerator;
	uint32_t Q_temp;
	for (register int i = 31; i >= 0; i--) {//Where n is number of bits in N
	  R = R << 1;//Left-shift R by 1 bit

	  bit_i_numerator = 0x0 | numerator;
	  bit_i_numerator = bit_i_numerator << (31 - i);
	  bit_i_numerator = bit_i_numerator >> 31;

	  R = R | (bit_i_numerator & 1);//Set the least-significant bit of R equal to bit i of the numerator
	  if (R >= denominator) {
	    R -= denominator;

	    Q_temp = Q;
	    Q = Q >> 31;
	    Q = Q | 1;
	    Q = Q << i;
	    Q = Q | Q_temp;//Q(i) = 1

	  }
	}

	return Q;
}